# Sass Boilerplate

This is a Sass boilerplate for front-end developers, using Gulp.js and Sass.

## Pre-requisite
- Clone this repo to your working directory
- Run `npm install` to install all dependencies
- Run `npm start` to start local development server on `localhost:3000` and watch changes for sass files in `src/scss/`

Happy coding!
